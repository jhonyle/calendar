;(function() {

/* different functions for work */
Date.prototype.daysInMonth = function( month ) {
    return 33 - new Date(this.getFullYear(), month || this.getMonth(), 33).getDate();
};

// http://javascript.ru/ui/offset
function getOffset(elem) {  
	function getOffsetSum(elem) {
	    var top=0, left=0;
	    while(elem) {
	        top = top + parseInt(elem.offsetTop);
	        left = left + parseInt(elem.offsetLeft);
	        elem = elem.offsetParent;
	    }
	    return {top: top, left: left}
	}

	function getOffsetRect(elem) {
	    var box = elem.getBoundingClientRect();

	    var body = document.body;
	    var docElem = document.documentElement;

	    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
	    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

	    var clientTop = docElem.clientTop || body.clientTop || 0;
	    var clientLeft = docElem.clientLeft || body.clientLeft || 0;

	    var top  = box.top +  scrollTop - clientTop;
	    var left = box.left + scrollLeft - clientLeft;

	    return { top: Math.round(top), left: Math.round(left) }
	}

    if (elem.getBoundingClientRect) {
        return getOffsetRect(elem);
    } else {
        return getOffsetSum(elem);
    }
}

function addEvent( elem, type, handler ) {
	if ( elem.addEventListener ) {
		elem.addEventListener( type, handler, false );
	} else {
		elem.attachEvent( 'on'+type, handler );
	}
}

function placeholder( elemId ) {
	var elem = document.getElementById( elemId ),
		placeholder = elem.getAttribute( 'placeholder' );

	if (elem.placeholder && 'placeholder' in document.createElement(elem.tagName)) return elem;
	
	if ( elem.value === '' ) {
		elem.setAttribute( 'value', placeholder );
	}

	addEvent( elem, 'focus', function () {
		if ( elem.value === placeholder ) {
			elem.value = '';
		}
	});

	addEvent( elem, 'blur', function () {
		if ( elem.value === '' ) {
			elem.value = placeholder;
		}
	});
}

if (!Array.prototype.indexOf) { 
    Array.prototype.indexOf = function(obj, start) {
         for (var i = (start || 0), j = this.length; i < j; i++) {
             if (this[i] === obj) { return i; }
         }
         return -1;
    }
}

function fireEvent( element, event ) {
    if ( document.createEventObject ) {
    	var evt = document.createEventObject( 'KeyboardEvent' );
    	return element.fireEvent( 'on' + event, evt );
    }
    else {
	    var evt = document.createEvent('HTMLEvents');
	    evt.initEvent(event, true, true );
	    return !element.dispatchEvent(evt);
    }
}

/* end of different functions for work */

function Calendar () {};

Calendar.prototype = (function () {
	var that = {
		constructor: Calendar,
		days: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресение'],
		months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Мая', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthsA: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],	
		bd: {}
	}

	function createMonth( year, month ) {
		var today = new Date(),
			iterator,
			lastMonth = new Date(),
			lastMonthLastDay = new Date(),
			nextMonth = new Date();

		if ( !that.bd[ year ] ) {
			that.bd[ year ] = {};
		}

		that.bd[ year ][ month ] = {};
		that.bd[ year ][ month ].thisMonth = {}; 
		that.bd[ year ][ month ].prevMonth = []; 
		that.bd[ year ][ month ].nextMonth = []; 

		today.setFullYear( year );
		today.setMonth( month );

		lastMonth.setFullYear( year );
		lastMonth.setMonth( today.getMonth() - 1 );
		nextMonth.setFullYear( year );
		nextMonth.setMonth( today.getMonth() + 1 );

		lastMonthLastDay.setMonth( today.getMonth() - 1 );
		lastMonthLastDay.setDate( lastMonth.daysInMonth() );
		if ( lastMonthLastDay.getDay() !== 7 ) {
			for (var i = 0, iterator = lastMonth.daysInMonth(), lengthFor = lastMonthLastDay.getDay(); i < lengthFor; iterator--, i++) {
				that.bd[ year ][ month ].prevMonth.unshift(iterator);
			}
		}

		for (iterator = 1; iterator <= today.daysInMonth(); iterator++) {
			that.bd[ year ][ month ][ 'thisMonth' ][ iterator ] = {};
			that.bd[ year ][ month ][ 'thisMonth' ][ iterator ].date = iterator;
		}

		var todayLast = new Date();
		todayLast.setFullYear( year );
		todayLast.setMonth( month );
		todayLast.setDate( iterator - 1 );

		if ( todayLast.getDay() !== 7 ) {
			for (var iterator = 1, lastDayOfThisList = 7 - todayLast.getDay(); iterator <= lastDayOfThisList; iterator++) {
				that.bd[ year ][ month ].nextMonth.push(iterator);
			}
		}
	}

	function drawMonth( year, month ) {
		var today = new Date(),
			lengthFor;

		clearCalendar();

		year = year || today.getFullYear();
		typeof month !== 'undefined' ? month = month : month = today.getMonth();

		that.currentYear = year;
		that.currentMonth = month;


		if ( !that.bd[ year ] ) {
		 	createMonth( year, month );
		}
		else if ( !that.bd[ year ][ month ] ) {
			createMonth( year, month );	
		}

		var calendarElements = document.createElement('div'),
			dayOfWeek = 0;

		function createDateOnCalendar ( info, eventOfThisDay ) {		
			var calendarDate,
				calendarDateTitle,
				eventWrap,
				eventTitle,
				eventMembers;

			calendarDate = document.createElement('div');
			calendarDate.className = 'calendar-date';
			if ( info.id ) { calendarDate.setAttribute( 'data-id', info.id ); }

			if ( info.activeDay ) { calendarDate.className += ' today'; }

			if ( info.positionDate ) { calendarDate.setAttribute( 'data-position', info.positionDate ); }  

			calendarDateTitle = document.createElement('div');
			calendarDateTitle.className = 'calendar-date-title';

			calendarDateTitle.innerHTML = info.title;
			if ( dayOfWeek < 7 ) { 
				calendarDateTitle.innerHTML = that.days[ dayOfWeek ] + ', ' + calendarDateTitle.innerHTML;
			}
			calendarDate.appendChild( calendarDateTitle );

			if ( eventOfThisDay ) {
				calendarDate.className += ' active';
				eventWrap = document.createElement('div');
				eventWrap.className = 'calendar-date-events';	
				
				eventTitle = document.createElement('div');
				eventTitle.className = 'calendar-date-events';	
				eventTitle.innerHTML = eventOfThisDay.title;
				eventWrap.appendChild( eventTitle );

				if ( 'eventMembers' in eventOfThisDay ) {
					eventMembers = document.createElement('div');
					eventMembers.className = 'calendar-date-members';
					eventMembers.innerHTML = eventOfThisDay.eventMembers;
					eventWrap.appendChild( eventMembers );
				}

				calendarDate.appendChild( eventWrap );
			}

			calendarElements.appendChild( calendarDate );
		}

		for (var j = 0; j < that.bd[ year ][ month ].prevMonth.length; j++, ++dayOfWeek) {
			createDateOnCalendar({
				title: that.bd[ year ][ month ][ 'prevMonth' ][j],
				positionDate: 'prev'
			});
		}

		for (j in that.bd[ year ][ month ].thisMonth) {
			if ( Object.prototype.hasOwnProperty.call( that.bd[ year ][ month ].thisMonth, j ) ) {
				if ( +today.getDate() === +j && (new Date()).getMonth() === month && (new Date()).getFullYear() === year ) {
					createDateOnCalendar( 
						{
							title: that.bd[ year ][ month ][ 'thisMonth' ][j].date,
							activeDay: true,
							id: j
						},
						that.bd[ year ][ month ][ 'thisMonth' ][j].eventOfThisDay					
					);					
				}
				else {
					createDateOnCalendar(
						{
							title: that.bd[ year ][ month ][ 'thisMonth' ][j].date,
							id: j
						},
						that.bd[ year ][ month ][ 'thisMonth' ][j].eventOfThisDay
					); 
				}
			}
			if ( dayOfWeek < 7 ) { dayOfWeek++ };
		}

		for (j = 0; j < that.bd[ year ][ month ].nextMonth.length; j++) {
			createDateOnCalendar({
				title: that.bd[ year ][ month ][ 'nextMonth' ][j],
				positionDate: 'next'
			});
		}

		document.getElementById('current-date-title').innerHTML = that.months[ month ] + ' ' + year;
		document.getElementById('calendar').innerHTML = calendarElements.innerHTML;
	}

	function clearCalendar() {
		document.getElementById('calendar').innerHTML = '';
	}

	function addFastEvent ( eventString ) {
		var eventArray = eventString.split(','),
			eventDate = eventArray[ 0 ].split(' '),
			year = (new Date()).getFullYear(),
			month = eventDate[ 1 ] ? that.monthsA.indexOf( eventDate[ 1 ].replace(' ', '') ) : -1;
		

		if ( !that.bd[ year ] || !that.bd[ year ][ month ] ) { createMonth( year, month ); }

		if (!( month !== -1 && eventDate[ 0 ] % 1 === 0 && eventDate[ 0 ] >= 1 && eventDate[ 0 ] <= 31)) {
			alert('Невозможно распознать введенные данные!');
			return;
		}

		if ( !eventArray[ 1 ] || eventArray[ 1 ].replace(/^\s/i, '') === '' ) {
			alert('Нельзя добавить событие без заголовка! Напишите его через запятую после даты.');
			return;
		}

		that.bd[ year ][ month ][ 'thisMonth' ][ +eventDate[ 0 ] ][ 'eventOfThisDay' ] = {};
		that.bd[ year ][ month ][ 'thisMonth' ][ +eventDate[ 0 ] ][ 'eventOfThisDay' ].month = month;
		that.bd[ year ][ month ][ 'thisMonth' ][ +eventDate[ 0 ] ][ 'eventOfThisDay' ].title = eventArray[ 1 ].replace(/^\s/i, '');

		drawMonth( year, month );

		document.getElementById('add-fast-event-window').style.display = 'none';
		document.getElementById('add-event-button').className = document.getElementById('add-event-button').className.replace('active', '');
	}

	function addFullEvent ( title, date, people, description ) {
		var year = that.currentYear,
			month = that.currentMonth;

		if ( !that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ] ) { 
			that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ] = {}; 
		}

		if ( typeof that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ].title === 'undefined' && title === '' ) {
			alert('Нельзя добавить событие без заголовка!');
			return;
		}

		if ( month ) { that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ].month = month; }
		if ( title ) { that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ].title = title; }
		if ( people ) { that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ].eventMembers = people; }
		if ( description ) { that.bd[ year ][ month ][ 'thisMonth' ][ date ][ 'eventOfThisDay' ].eventDescription = description; }

		drawMonth( year, month );

		document.getElementById('add-full-event-window').style.display = 'none';
	}

	function deleteEvent ( year, month, day ) {
		year = year || that.currentYear;
		month = month || that.currentMonth;
		day = day || that.currentDateClickId;

		delete that.bd[ year ][ month ][ 'thisMonth' ][ day ][ 'eventOfThisDay' ];
		document.getElementById('add-full-event-window').style.display = 'none';
		drawMonth( year, month );
	}

	function showAddFullEventWindow ( target, event ) {
		if ( target.getAttribute( 'data-id' ) ) {
			var mouseX = event.clientX,
				mouseY = event.clientY,
				documentWidth = document.body.offsetWidth,
				documentHeight = document.body.offsetHeight,
				addEventWindow = document.getElementById('add-full-event-window'),
				windowClassName,
				currentEvent;

			addEventWindow.style.display = 'none';

			that.currentDateClickId = target.getAttribute( 'data-id' );

			if ( mouseX <= 450 ) {
				windowClassName = 'left-';
				addEventWindow.style.left = getOffset( target ).left + target.offsetWidth + 'px';
			}
			else {
				windowClassName = 'right-';
				addEventWindow.style.left = getOffset( target ).left + 'px';
			}

			if ( mouseY < 300 ) {
				windowClassName += 'top';
				addEventWindow.style.top = getOffset( target ).top + 'px';
			}
			else {
				windowClassName += 'bottom';
				addEventWindow.style.top = getOffset( target ).top + 'px';
			}

			addEventWindow.className = windowClassName;
			currentEvent = that.bd[ that.currentYear ][ that.currentMonth ][ 'thisMonth' ][ that.currentDateClickId ][ 'eventOfThisDay' ];
			
			if ( currentEvent ) {
				if ( currentEvent.title ) {
					document.getElementById('add-full-event-title').style.display = 'none';
					document.getElementById('add-full-event-title-content').style.display = 'block';
					document.getElementById('add-full-event-title-content').innerHTML = currentEvent.title;
				}

				if ( currentEvent.eventMembers ) {
					document.getElementById('add-full-event-people').style.display = 'none';
					document.getElementById('add-full-event-people-content').style.display = 'block';
					document.getElementById('add-full-event-people-content-people').innerHTML = currentEvent.eventMembers;
				}

				if ( currentEvent.eventDescription ) {
					document.getElementById('add-full-event-description').style.display = 'none';
					document.getElementById('add-full-event-description-content').style.display = 'block';
					document.getElementById('add-full-event-description-content').innerHTML = currentEvent.eventDescription;
				}
			}
			else {
				clearInput( document.getElementById('add-full-event-title') );
				document.getElementById('add-full-event-title').style.display = 'block';
				document.getElementById('add-full-event-title-content').style.display = 'none';
				
				clearInput( document.getElementById('add-full-event-people') );
				document.getElementById('add-full-event-people').style.display = 'block';
				document.getElementById('add-full-event-people-content').style.display = 'none';

				clearInput( document.getElementById('add-full-event-description') );
				document.getElementById('add-full-event-description').style.display = 'block';
				document.getElementById('add-full-event-description-content').style.display = 'none';
			}

			addEventWindow.style.display = 'block';
		}
		else {
			if ( target.getAttribute( 'data-position' ) === 'prev' ) {
				prevMonth();
			}
			else if ( target.getAttribute( 'data-position' ) === 'next' ) {
				nextMonth();
			}
		}		
	}

	function prevMonth() {
		document.getElementById('add-full-event-window').style.display = 'none';
		drawMonth( that.currentMonth !== 0 ? that.currentYear : that.currentYear - 1, that.currentMonth !== 0 ? that.currentMonth - 1 : 11 );
	}

	function nextMonth() {	
		document.getElementById('add-full-event-window').style.display = 'none';
		drawMonth( that.currentMonth !== 11 ? that.currentYear : that.currentYear + 1, that.currentMonth !== 11 ? that.currentMonth + 1 : 0 );	
	}

	function searchEvents ( searchString ) {
		var thisMonth = that.bd[ that.currentYear ][ that.currentMonth ][ 'thisMonth' ],
			results = [],
			iterator = 0,
			searchValue = new RegExp( searchString, 'i' );

		for ( iterator in thisMonth ) {
			if ( Object.prototype.hasOwnProperty.call( thisMonth, iterator ) ) {
				if ( thisMonth[ iterator ].eventOfThisDay ) {
					if ( thisMonth[ iterator ].eventOfThisDay.title && searchValue.test( thisMonth[ iterator ].eventOfThisDay.title ) || thisMonth[ iterator ].eventOfThisDay.eventMembers && searchValue.test( thisMonth[ iterator ].eventOfThisDay.eventMembers ) || thisMonth[ iterator ].eventOfThisDay.eventDescription && searchValue.test( thisMonth[ iterator ].eventOfThisDay.eventDescription ) ) {
						results.push( thisMonth[ iterator ] );
					}
				}
			}
		}

		return results;
	}

	function clearInput ( elem ) {
		if (elem.placeholder && 'placeholder' in document.createElement(elem.tagName)) { 
			elem.value = '';
		}
		else {
			elem.value = elem.getAttribute( 'placeholder' );
		}	
	}

	that.initialization = function () {
		placeholder( 'add-fast-event-text' );
		placeholder( 'search-event-input' );
		placeholder( 'add-full-event-title' );
		placeholder( 'add-full-event-people' );
		placeholder( 'add-full-event-description' );

		addEvent( document.getElementById('add-event-button'), 'click', function () {
			if ( /active/i.test(this.className) ) {
				document.getElementById('add-fast-event-window').style.display = 'none';
				this.className = this.className.replace(' active', '');
			}
			else {
				document.getElementById('add-fast-event-window').style.display = 'block';
				this.className += ' active';
			}
		});

		addEvent( document.getElementById('add-fast-event-exit'), 'click', function () {
			document.getElementById('add-fast-event-window').style.display = 'none';
			document.getElementById('add-event-button').className = document.getElementById('add-event-button').className.replace('active', '');
		});

		addEvent( document.getElementById('load-prev-month'), 'click', prevMonth);

		addEvent( document.getElementById('load-next-month'), 'click', nextMonth);

		addEvent( document.getElementById('go-to-today'), 'click', function () { drawMonth(); } );

		addEvent( document.getElementById('add-fast-event-button'), 'click', function () {
			addFastEvent( document.getElementById('add-fast-event-text').value );
		});

		addEvent( document.getElementById('add-full-event-exit'), 'click', function() {
			clearInput( document.getElementById('add-full-event-title') );
			clearInput( document.getElementById('add-full-event-people') );
			clearInput( document.getElementById('add-full-event-description') );
			document.getElementById('add-full-event-window').style.display = 'none';
		});

		addEvent( document.getElementById('calendar'), 'click', function ( event ) {
			event = event || window.event;

			var target = event.target || event.srcElement;

			if ( /calendar-date/i.test( target.className ) ) {
				showAddFullEventWindow ( target, event );
			}

			if ( /calendar-date-title/i.test( target.className ) || /calendar-date-events/i.test( target.className ) ) {
				showAddFullEventWindow ( target.parentNode, event );
			}

			if ( /calendar-date-events/i.test( target.className ) || /calendar-date-members/i.test( target.className ) ) {
				showAddFullEventWindow ( target.parentNode.parentNode, event );
			}

			return false;
		});

		addEvent( document.getElementById('add-full-event-ok'), 'click', function ( event ) {
			addFullEvent(
				document.getElementById('add-full-event-title').value !== document.getElementById('add-full-event-title').getAttribute('placeholder') ? document.getElementById('add-full-event-title').value : '',
				that.currentDateClickId,
				document.getElementById('add-full-event-people').value !== document.getElementById('add-full-event-people').getAttribute('placeholder') ? document.getElementById('add-full-event-people').value : '',
				document.getElementById('add-full-event-description').value !== document.getElementById('add-full-event-description').getAttribute('placeholder') ? document.getElementById('add-full-event-description').value : ''
			);

			clearInput( document.getElementById('add-full-event-title') );
			clearInput( document.getElementById('add-full-event-people') );
			clearInput( document.getElementById('add-full-event-description') );
		});

		addEvent( document.getElementById('add-full-event-delete'), 'click', function () {
			deleteEvent();
			clearInput( document.getElementById('add-full-event-title') );
			clearInput( document.getElementById('add-full-event-people') );
			clearInput( document.getElementById('add-full-event-description') );
		});

		addEvent( document.getElementById('search-event-input'), 'keyup', function ( event ) {
			event = event || window.event;

			var variants = searchEvents( this.value ),
				wrapper = document.getElementById('search-variants'),
				variantElem,
				title,
				date,
				hr,
				i, j;

			wrapper.innerHTML = '';

			for ( i = 0; i < variants.length; i++ ) {
				variantElem = document.createElement('div'),
				title = document.createElement('div'),
				date = document.createElement('div'),
				hr = document.createElement('hr');

				variantElem.className = 'header-search-form-variant';
				title.className = 'header-search-form-variant-title';
				date.className = 'header-search-form-variant-date';
				hr.className = 'header-search-form-line';

				variantElem.setAttribute( 'data-id', variants[ i ].date )
				title.innerHTML = variants[ i ].eventOfThisDay.title;
				date.innerHTML = variants[ i ].date + ' ' + that.monthsA[ +variants[ i ].eventOfThisDay.month ];

				variantElem.appendChild( title );
				variantElem.appendChild( date );
				wrapper.appendChild( variantElem );

				if ( i !== variants.length - 1) {
					wrapper.appendChild( hr );
				}
			}

			document.getElementById('search-form-variants').style.display = 'block';

		});

		addEvent( document.getElementById('search-variants'), 'click', function ( event ) {
			event = event || window.event;

			var target = event.target || event.srcElement,
				dId = target.getAttribute( 'data-id' ) || target.parentNode.getAttribute( 'data-id' );

			if ( /header-search-form-variant/i.test( target.className ) ) {	
				var forActiveDate = document.getElementsByClassName('calendar-date');
				for ( j = 0; j < forActiveDate.length; j++ ) {
					if ( forActiveDate[ j ].getAttribute( 'data-id' ) && forActiveDate[ j ].getAttribute( 'data-id' ) === dId ) {
			
						forActiveDate[ j ].click(  );
						document.getElementById('search-form-variants').style.display = 'none';
					}
				}
			}

			return false;
		});

		addEvent( document.getElementById('search-event-input'), 'focus', function ( event ) {			
			document.getElementById('search-form-variants').style.display = 'block';
		});

		function stopProg ( event ) {
			event = event || window.event;

			if ( event.stopPropagation ) {
				event.stopPropagation();
			}
			else {
				event.canselBubble = true;
			}			
		}

		addEvent( document.getElementById('search-event-input'), 'click', function ( event ) {
			event = event || window.event;

			if ( event.stopPropagation ) {
				event.stopPropagation();
			}
			else {
				event.canselBubble = true;
			}

			fireEvent( this, 'keyup' );
		});

		addEvent( document.getElementById('search-form-variants'), 'click',  stopProg);		

		addEvent( document.getElementsByTagName('body')[0], 'click', function ( event ) {
			document.getElementById('search-form-variants').style.display = 'none';
		});

		drawMonth();
	}

	return that;

})();


window.onload = function() {
	var calendar = new Calendar();
	calendar.initialization();
}

})();